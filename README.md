**Pour contribuer à ce projet :**

* Ajouter une clef ssh sur son compte dans profil ->  paramètres -> clés SSH (menu de gauche)
* Dans le dossier idoine, faire git clone git+ssh://gogs@code.ffdn.org:55555/gitoyen/communication.git
* Opérer les changements nécessaires dans le dossier communication 
* git status
* git add des fichiers modifiés que l’on veut envoyer (fonctionne avec la complétion)
* git commit -m "message"
* git push
* si on obtient le message "Gogs: You do not have sufficient authorization for this action", demander à être rajouté en tant que contributeur·rice ;-)
